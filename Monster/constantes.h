#ifndef CONSTANTES_H
#define CONSTANTES_H

//Constantes
const int ECRAN_WIDTH = 320; //largeur de l'écran
const int ECRAN_HEIGHT = 568;//hauteur de l'écran
const int ECRAN_BPP = 32;// pixels par pouces de l'écran
const int SIZE_CELL = 67;// taille d'un rectangle de lecture pour les éléments
const int SIZE_FLECHE = 60;// taille d'un rectangle de lecture pour les flèches
const int nbLigne = 9;// nombre de lignes
const int nbColonne = 5; // nombre de colonnes
const int leftBorder = 10;// différence entre le début du damier et le bord de l'image
const int topBorder = 33;// différence entre le début du damier et le bord de l'image
const int CASE_HEIGHT = 53;// hauteur d'une case
const int CASE_WIDTH = 57;// largeur d'une case
const int gap = 10;// espace entre deux cases


#endif // CONSTANTES_H
