#ifndef _LEVEL_H_
#define _LEVEL_H_

#include <iostream>
#include <array>
#include "monster.h"
#include "constantes.h"

using namespace std;

using TGrille = array<array<int,nbColonne>,nbLigne>;


//Définition des prototypes de fonctions
void initGrille(TGrille & grille);
void afficherGrille(TGrille &grille, Element &e, SDL_Surface *screen);
void initLvl(TGrille &grille, int &lvl);
void selecCase(SDL_Event event, int &l, int &c);
bool compareGrille(int tmp_l, int tmp_c, TGrille &grille);
void decalageDroite(TGrille & grille, int &l, int &c, bool &droite, bool &bas);
void decalageGauche(TGrille & grille, int &l, int &c, bool &gauche, bool &bas);
void decalageHaut(TGrille & grille, int &l, int &c, bool &haut);
void decalageBas(TGrille & grille, int &l, int &c, bool &bas);
bool lvlFini(TGrille grille);
void gameOver(TGrille &grille, int l, int c, int lvl, bool haut, bool bas, bool droite, bool gauche);
void glisser(TGrille &grille, int &l, int &c, int &endX, int &endY, int &startX, int &startY, bool &droite, bool &bas, bool &gauche, bool &haut );
#endif
