#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "grille.h"
#include "constantes.h"

using namespace std;

/****************** Nom de la fonction **********************
* lectureElement                                            *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 28/12/16                                *
********************* Description ***************************
* Initialise graphiquement les monstres                     *
*********************** Entrées *****************************
* Element : structure, int x, y: position des monstres      *
*********************** Sorties *****************************
* Renvoie e                                                 *
************************************************************/
void lectureElement(Element & e, int x, int y)
{
    e.x = leftBorder + y * CASE_WIDTH;
    e.y = (topBorder + x * CASE_HEIGHT)-gap; // calcul de la position de l'élément en fonction du début de la grille et du bord de l'image ainsi que de la hauteur de la case moins l'espace entre les cases



    e.lecture_standard.x = 0;
    e.lecture_standard.y = 140;
    e.lecture_standard.w = SIZE_CELL;
    e.lecture_standard.h = SIZE_CELL;



    e.lecture_dormeur.x = 2;
    e.lecture_dormeur.y = 1;
    e.lecture_dormeur.w = SIZE_CELL;
    e.lecture_dormeur.h = SIZE_CELL;



    e.lecture_livre.x = 64;
    e.lecture_livre.y = 70;
    e.lecture_livre.w = SIZE_CELL;
    e.lecture_livre.h = SIZE_CELL;




    e.lecture_glace.x = 0;
    e.lecture_glace.y = 73;
    e.lecture_glace.w = SIZE_CELL;
    e.lecture_glace.h = SIZE_CELL;



    e.lecture_dead.x = 250;
    e.lecture_dead.y = 150;
    e.lecture_dead.w = SIZE_CELL;
    e.lecture_dead.h = SIZE_CELL;

    e.lecture_fleche.x = 0;
    e.lecture_fleche.y = 210;
    e.lecture_fleche.w = SIZE_FLECHE;
    e.lecture_fleche.h = SIZE_FLECHE;


}

