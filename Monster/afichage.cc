#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "grille.h"
#include "constantes.h"


using namespace std;

// -- loadImage ---------------------------------
// chargement d'une image
// * paramètres entrées :
// - "filename" : nom de l'image
// * paramètre de sortie : SDL_Surface contenant
//   l'image.
// Auteur : IUT
// ----------------------------------------------
SDL_Surface *load_image( string filename )
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
    }
    return optimizedImage;
}


// -- applySurface ------------------------------
// c'est le copier-coller d'une surface sur une
// autre : on colle le rectangle "clip" de "source"
// sur "destination" à partir de "x,y
// Auteur : IUT
// ----------------------------------------------
void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, clip, destination, &offset );
}


// -- loadImageWithColorKey ---------------------
// chargement d'une image
// * paramètres entrées :
// - "filename" : nom de l'image
// - "r,g,b"    : couleur de la ColorKey; cette
//   couleur devient transparente !
// * paramètre de sortie : SDL_Surface contenant
//   l'image.
// Auteur : IUT
// ----------------------------------------------
SDL_Surface *loadImageWithColorKey(string filename, int r, int g, int b)
{

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );

    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
        if( optimizedImage != NULL )
        {
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, r, g, b );
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }

    return optimizedImage;
}

/****************** Nom de la fonction **********************
* button                                                    *
******************** Auteur , Dates *************************
* SATGER / 28/11/16                                         *
********************* Description ***************************
* Cette fonction initialise les SDL_Rect permettant de      *
* cliquer sur les boutons correspondant                     *
*********************** Entrées *****************************
* 3 SDL_Rect                                                *
*********************** Sorties *****************************
* Cette fonction ne renvoie rien                            *
************************************************************/
void button(SDL_Rect &playButt, SDL_Rect &helpButt, SDL_Rect &quitButt, SDL_Rect &reinitButt)
{
    playButt.x=115;
    playButt.y=280;
    playButt.w=70;
    playButt.h=70;

    helpButt.x=203;
    helpButt.y=355;
    helpButt.w=35;
    helpButt.h=35;

    quitButt.x=185;
    quitButt.y=225;
    quitButt.w=35;
    quitButt.h=35;

    reinitButt.x=68;
    reinitButt.y=506;
    reinitButt.w=53;
    reinitButt.h=53;
}

/****************** Nom de la fonction **********************
* affichMenu                                                *
******************** Auteur , Dates *************************
* SATGER / 09/11/16                                         *
********************* Description ***************************
* Cette fonction initialise le menu et les boutons          *
*********************** Entrées *****************************
* SDL_Event &event, bool &quit, int &jeu, SDL_Surface
* *screen, SDL_Rect playButt, SDL_Rect helpButt, SDL_Rect
* quitButt, SDL_Surface *play, SDL_Surface *help,
* SDL_Surface *quitt                                        *
*********************** Sorties *****************************
* quitte le jeu si quit est vraie                           *
************************************************************/
void affichMenu(SDL_Event event, bool &quit, int &jeu, SDL_Surface *screen, SDL_Rect playButt, SDL_Rect helpButt, SDL_Rect quitButt, SDL_Surface *play, SDL_Surface *help, SDL_Surface *quitt)
{
    int x = event.button.x;
    int y = event.button.y;

    if( event.type == SDL_QUIT )
    {
        quit = true;
    }
    if ( event.type == SDL_MOUSEMOTION)
    {
        if ( ( x > playButt.x ) && ( x < playButt.x + playButt.w ) && ( y > playButt.y ) && ( y < playButt.y + playButt.h ) ) // "hit-box" du bouton play
        {
            applySurface(0,0,play,screen,NULL);
        }
        if( ( x > helpButt.x ) && ( x < helpButt.x + helpButt.w ) && ( y > helpButt.y ) && ( y < helpButt.y + helpButt.h ) )
        {
            applySurface(0,0,help,screen,NULL);
        }
        if( ( x > quitButt.x ) && ( x < quitButt.x + quitButt.w ) && ( y > quitButt.y ) && ( y < quitButt.y + quitButt.h ) )
        {
            applySurface(0,0,quitt,screen,NULL);
        }
    }

    if( event.type == SDL_MOUSEBUTTONDOWN )
    {
        if ( ( x > playButt.x ) && ( x < playButt.x + playButt.w ) && ( y > playButt.y ) && ( y < playButt.y + playButt.h ) )
        {
            jeu = 1;

        }

        if( ( x > quitButt.x ) && ( x < quitButt.x + quitButt.w ) && ( y > quitButt.y ) && ( y < quitButt.y + quitButt.h ) )
        {
            quit = true;
        }
    }
}
