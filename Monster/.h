#ifndef _MONSTER_H_
#define _MONSTER_H_

#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>

enum EnumObstacle {LIVRE,GLACE};

//Constantes
const int ECRAN_WIDTH = 320;
const int ECRAN_HEIGHT = 568;
const int ECRAN_BPP = 32;
const int NB_LEVEL = 6;


// Définitions des structures
struct Obstacle
{
    EnumObstacle typeObstacle;
    int caseX; //entre 0 et 5
    int caseY;// entre 0 et 9
};


//Définitions des prototypes de fonctions
void initObstacle(Monster & mR);

#endif
