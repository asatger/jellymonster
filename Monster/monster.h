#ifndef _MONSTER_H_
#define _MONSTER_H_

#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>


enum enumElement {DEAD,STANDARD,DORMEUR,LIVRE,GLACE,FLECHE}; // DEAD = rien, STANDARD = monstre réveillé, DORMEUR = monstre à réveiller, FLECHE = fleche allant vers le bas

// Définitions des structures
struct Element
{
    int x; // coordonnées de l'élément
    int y;
    SDL_Surface *element;
    SDL_Rect lecture_standard, lecture_dormeur, lecture_glace, lecture_livre, lecture_dead, lecture_courant, lecture_actuelle, lecture_fleche;
};

//Définitions des prototypes de fonctions
void lectureElement(Element & e, int x, int y);

#endif
