#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include <stdlib.h>

#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "grille.h"
#include "constantes.h"

void jeu0(SDL_Surface *menu, SDL_Surface *screen, SDL_Event &event, bool &quit, int &jeu, SDL_Rect playButt, SDL_Rect helpButt, SDL_Rect quitButt, SDL_Surface *play, SDL_Surface *help,
          SDL_Surface *quitt, int &lvl, TGrille &grille);

void jeu1(TGrille &grille, int &jeu, Element &e, bool &haut, bool &bas, bool &gauche, bool &droite, bool &monstreClique, int &l, int &c, int &lvl, SDL_Surface *fin, SDL_Surface *background,
          SDL_Surface *screen, SDL_Event &event, int &startX, int &startY, int &endX, int &endY, SDL_Rect reinitButt, SDL_Surface *win);
#endif // JEU_H
