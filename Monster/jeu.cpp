using namespace std;
#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "constantes.h"
#include "jeu.h"
#include <iostream>

/****************** Nom de la fonction **********************
* jeu0                                            *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 14/12/16                      *
********************* Description ***************************
* Cette fonction initialise le menu                         *
*********************** Entrées *****************************
* SDL_Surface: pour afficher les images du menu,            *
* grille: grille du jeu,
* SDL_Event: gère les evenements,
* bool quit: pour quitter le jeu,
* int jeu: etat de jeu,
* SDL_Rect: rectangle de lecture des boutons du menu,
* int lvl: niveau
*********************** Sorties *****************************
* Cette fonction ne renvoie rien                            *
************************************************************/
void jeu0(SDL_Surface *menu, SDL_Surface *screen, SDL_Event &event, bool &quit, int &jeu, SDL_Rect playButt, SDL_Rect helpButt, SDL_Rect quitButt, SDL_Surface *play, SDL_Surface *help,
          SDL_Surface *quitt, int &lvl, TGrille &grille)
{
    applySurface(0,0,menu,screen,NULL);
    affichMenu(event,quit,jeu,screen,playButt,helpButt,quitButt,play,help,quitt); // affiche le menu
    lvl = 1; // permet de commencer par le premier niveau
    initLvl(grille, lvl); // initialise le niveau

}

/****************** Nom de la fonction **********************
* jeu1                                                      *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 14/12/16                                *
********************* Description ***************************
* Cette fonction initialise le jeu                          *
*********************** Entrées *****************************
* TGrille: grille de jeu                                    *
* int jeu: etat de jeu,
* Element e: structure des éléments,
* bool haut, bas, gauche, droite: direction des monstres,
* int l et c: ligne et colonne du monstre sélectionné,
* SDL_Surface: pour afficher les images du jeu
* startX, startY: position de début du glissé de la souris
* endX, endY: position de fin du glissé de la souris*
* SDL_Rect: rectangle de lecture du bouton "reinitialiser"
*********************** Sorties *****************************
* Cette fonction ne renvoie rien                            *
************************************************************/
void jeu1(TGrille &grille, int &jeu, Element &e, bool &haut, bool &bas, bool &gauche, bool &droite, bool &monstreClique, int &l, int &c, int &lvl, SDL_Surface *fin, SDL_Surface *background,
          SDL_Surface *screen, SDL_Event &event, int &startX, int &startY, int &endX, int &endY, SDL_Rect reinitButt, SDL_Surface *win)
{
    applySurface(0,0,background,screen,NULL);

    while(SDL_PollEvent(&event))
    {
        int x = event.button.x; // coordonnées de la souris
        int y = event.button.y;

        if (event.type == SDL_KEYDOWN)
        {
            if(event.key.keysym.sym == SDLK_ESCAPE)
            {
                jeu = 0; // retour en menu si touche espace enfoncée
            }
        }

        if (event.type == SDL_MOUSEBUTTONDOWN)
        {
            l = c = 0; // ligne et colonne initialisées à 0 à chaque nouveau tour de boucle
            selecCase( event, l, c);

            if(compareGrille(l, c, grille))
            {
                startX = event.button.x;
                startY = event.button.y;
                monstreClique = true;
            }

            if ( ( x > reinitButt.x ) && ( x < reinitButt.x + reinitButt.w ) && ( y > reinitButt.y ) && ( y < reinitButt.y + reinitButt.h ) )
            {
                initLvl(grille, lvl);
            }
        }

        if( event.type == SDL_MOUSEBUTTONUP && monstreClique)
        {
            endX = event.button.x;
            endY = event.button.y;
            monstreClique = false; // vérifie si un monstre a été sélectionné par un clic de souris
        }
    }

    if(endX != -1 && endY != -1)
    {
        glisser(grille, l, c, endX, endY, startX, startY, droite, bas, gauche, haut );

        gameOver(grille, l, c, lvl, haut, bas, droite, gauche);

        if (lvlFini(grille)) // fin du niveau
        {
            lvl ++; // passage au niveau suivant
            initLvl(grille, lvl);
            jeu = 2; // écran de transition
        }

        endX = endY = -1; // réinitailiastion des coordonnées de fin de cliquer-glisser de la souris pour éviter de garder celles du niveau précédent
        gauche = droite = haut = bas = false;
    }

    afficherGrille(grille, e, screen);

    if (lvlFini(grille))
    {
        jeu = 3;
    }
    if (jeu == 2)
    {
        applySurface(0,0,win,screen,NULL);
    }

    if (jeu == 3)
    {
        applySurface(0,0,fin,screen,NULL);
    }
}

