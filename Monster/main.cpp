//Projet Monster IUT Informatiaque Bordeaux
//Copyright (C) 2016  SATGER - MESLIN

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>

#include <iostream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include <stdlib.h>

#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "grille.h"
#include "constantes.h"
#include "jeu.h"

using namespace std;

int main()
{
    TGrille grille;

    bool quit = false;
    int jeu = 0 ;

    // Définition des surfaces
    SDL_Surface *screen;
    SDL_Surface *menu;
    SDL_Surface *play;
    SDL_Surface *help;
    SDL_Surface *quitt;
    SDL_Surface *background;
    SDL_Surface *fin;
    SDL_Surface *win;

    SDL_Event event;

    SDL_Rect playButt;
    SDL_Rect helpButt;
    SDL_Rect quitButt;
    SDL_Rect reinitButt;
    button(playButt,helpButt,quitButt,reinitButt);
    Element e;

    SDL_Init(SDL_INIT_EVERYTHING);
    screen=SDL_SetVideoMode(ECRAN_WIDTH,ECRAN_HEIGHT, ECRAN_BPP,SDL_SWSURFACE);

    menu=load_image("menu.bmp");
    play=load_image("jouer.bmp");
    help=load_image("aide.bmp");
    quitt=load_image("quitter.bmp");
    background=load_image("background.bmp");
    e.element=loadImageWithColorKey("sprite.bmp",255,255,255);
    fin=load_image("winEndSprite.bmp");
    win=load_image("winSprite.bmp");

    int lvl;
    while (!quit)
    {
        int l = 0;
        int c = 0;

        int startX=-1, startY=-1, endX=-1, endY=-1; //coordonnées de la souris avant et après déplacement
        bool monstreClique = false;
        bool droite, gauche, haut, bas;

        while (jeu == 0 && !quit)
        {
            while(SDL_PollEvent(&event))
            {
                jeu0(menu, screen, event, quit, jeu, playButt, helpButt, quitButt, play, help, quitt, lvl, grille); //gestion des fonctions relatives au menu
                SDL_Flip(screen);
            }
        }

        while (jeu == 1 && !quit)
        {
            jeu1(grille, jeu, e, haut, bas, gauche, droite, monstreClique, l, c, lvl, fin, background,
                 screen, event, startX, startY, endX, endY, reinitButt, win); //gestion des fonctions relatives au jeu
            SDL_Flip(screen);
        }


        if (jeu == 2) //transition entre les niveaux
        {
            jeu = 1;
            SDL_Delay(1000); // affichage pendant 1 seconde de l'image de transition
        }

        if (jeu == 3) //fin du jeu
        {
            jeu = 0;
            SDL_Delay(2000); // affichage pendant 2 secondes de l'image de fin de jeu
        }

        if (jeu == -1)
        {
            quit = true;
        }

        SDL_Delay(10);
    }

    SDL_FreeSurface(fin);
    SDL_FreeSurface(e.element);
    SDL_FreeSurface(menu);
    SDL_FreeSurface(play);
    SDL_FreeSurface(help);
    SDL_FreeSurface(background);

    SDL_Quit();

    return EXIT_SUCCESS;
}
