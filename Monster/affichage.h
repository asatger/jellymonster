#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
using namespace std;
SDL_Surface *load_image(string filename );
SDL_Surface *loadImageWithColorKey(string filename, int r, int g, int b);


using TGrille = array<array<int,nbColonne>,nbLigne>;


void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);

void button(SDL_Rect &playbutt, SDL_Rect &helpButt, SDL_Rect &quitButt, SDL_Rect &reinitButt);

void affichMenu(SDL_Event event, bool &quit, int &jeu, SDL_Surface *screen, SDL_Rect playButt, SDL_Rect helpButt, SDL_Rect quitButt, SDL_Surface *play, SDL_Surface *help, SDL_Surface *quitt);





#endif // AFFICHAGE_H
