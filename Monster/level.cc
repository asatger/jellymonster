using namespace std;
#include "level.h"
#include "affichage.h"
#include "monster.h"
#include "constantes.h"
#include <iostream>



// Fonctions



/****************** Nom de la fonction **********************
* initGrille                                             *
******************** Auteur , Dates *************************
* SATGER / 26/11/16                      *
********************* Description ***************************
* Cette fonction initialise la matrice du jeu avec des      *
* monstres de types DEAD                                    *
*********************** Entrées *****************************
* Prend en entrée un tableau de tableau (grille)            *
*********************** Sorties *****************************
* Renvoie la grille                           *
************************************************************/
void initGrille(TGrille  &grille)
{
    int l, c;
    for (l = 0 ; l < nbLigne ;l++)
    {
        for (c = 0 ; c < nbColonne ; c++)
        {
            grille[l][c] = DEAD;
        }
    }
}


/****************** Nom de la fonction **********************
* initLvl1                                                  *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 01/12/16                                *
********************* Description ***************************
* Cette fonction initialise la grille du niveau             *
*********************** Entrées *****************************
* TGrille &grille, structure Element, SDL_Surface screen,
* SDL_Event &event
*********************** Sorties *****************************
* Renvoie la grille et le niveau                            *
************************************************************/
void initLvl(TGrille &grille, int &lvl)
{
    initGrille(grille);

    if(lvl == 1)
    {

        grille[0][0] = DEAD;
        grille[0][1] = DEAD;
        grille[0][2] = DEAD;
        grille[0][3] = DEAD;
        grille[0][4] = DEAD;

        grille[1][0] = DEAD;
        grille[1][1] = DEAD;
        grille[1][2] = DEAD;
        grille[1][3] = DEAD;
        grille[1][4] = DEAD;

        grille[2][0] = DEAD;
        grille[2][1] = DEAD;
        grille[2][2] = DEAD;
        grille[2][3] = DEAD;
        grille[2][4] = DEAD;

        grille[3][0] = DEAD;
        grille[3][1] = DEAD;
        grille[3][2] = DEAD;
        grille[3][3] = DEAD;
        grille[3][4] = DEAD;

        grille[4][0] = STANDARD;
        grille[4][1] = DEAD;
        grille[4][2] = DEAD;
        grille[4][3] = DEAD;
        grille[4][4] = DORMEUR;

        grille[5][0] = DEAD;
        grille[5][1] = DEAD;
        grille[5][2] = DEAD;
        grille[5][3] = DEAD;
        grille[5][4] = DEAD;

        grille[6][0] = DEAD;
        grille[6][1] = DEAD;
        grille[6][2] = DEAD;
        grille[6][3] = DEAD;
        grille[6][4] = DEAD;

        grille[7][0] = DEAD;
        grille[7][1] = DEAD;
        grille[7][2] = DEAD;
        grille[7][3] = DEAD;
        grille[7][4] = DEAD;

        grille[8][0] = DEAD;
        grille[8][1] = DEAD;
        grille[8][2] = DEAD;
        grille[8][3] = DEAD;
        grille[8][4] = DEAD;
    }

    if(lvl == 2)
    {

        grille[0][0] = DEAD;
        grille[0][1] = DEAD;
        grille[0][2] = DEAD;
        grille[0][3] = DEAD;
        grille[0][4] = DEAD;

        grille[1][0] = DEAD;
        grille[1][1] = DEAD;
        grille[1][2] = DEAD;
        grille[1][3] = DEAD;
        grille[1][4] = DEAD;

        grille[2][0] = DEAD;
        grille[2][1] = STANDARD;
        grille[2][2] = DEAD;
        grille[2][3] = FLECHE;
        grille[2][4] = DEAD;

        grille[3][0] = DEAD;
        grille[3][1] = DEAD;
        grille[3][2] = DEAD;
        grille[3][3] = DEAD;
        grille[3][4] = DEAD;

        grille[4][0] = DEAD;
        grille[4][1] = DEAD;
        grille[4][2] = DEAD;
        grille[4][3] = GLACE;
        grille[4][4] = DEAD;

        grille[5][0] = DEAD;
        grille[5][1] = DEAD;
        grille[5][2] = DEAD;
        grille[5][3] = DEAD;
        grille[5][4] = DEAD;

        grille[6][0] = DEAD;
        grille[6][1] = DEAD;
        grille[6][2] = DEAD;
        grille[6][3] = DEAD;
        grille[6][4] = DEAD;

        grille[7][0] = DEAD;
        grille[7][1] = DEAD;
        grille[7][2] = DEAD;
        grille[7][3] = DEAD;
        grille[7][4] = DEAD;

        grille[8][0] = DEAD;
        grille[8][1] = DEAD;
        grille[8][2] = DEAD;
        grille[8][3] = DORMEUR;
        grille[8][4] = DEAD;
    }

    if(lvl == 3)
    {

        grille[0][0] = DEAD;
        grille[0][1] = DEAD;
        grille[0][2] = DEAD;
        grille[0][3] = DEAD;
        grille[0][4] = DEAD;

        grille[1][0] = DEAD;
        grille[1][1] = LIVRE;
        grille[1][2] = DEAD;
        grille[1][3] = DEAD;
        grille[1][4] = DEAD;

        grille[2][0] = DEAD;
        grille[2][1] = DEAD;
        grille[2][2] = DEAD;
        grille[2][3] = DEAD;
        grille[2][4] = DORMEUR;

        grille[3][0] = DEAD;
        grille[3][1] = DEAD;
        grille[3][2] = DEAD;
        grille[3][3] = DEAD;
        grille[3][4] = DEAD;

        grille[4][0] = DEAD;
        grille[4][1] = STANDARD;
        grille[4][2] = DEAD;
        grille[4][3] = DEAD;
        grille[4][4] = DEAD;

        grille[5][0] = DEAD;
        grille[5][1] = DEAD;
        grille[5][2] = DEAD;
        grille[5][3] = DEAD;
        grille[5][4] = DEAD;

        grille[6][0] = DEAD;
        grille[6][1] = DEAD;
        grille[6][2] = DEAD;
        grille[6][3] = DEAD;
        grille[6][4] = DEAD;

        grille[7][0] = DEAD;
        grille[7][1] = DEAD;
        grille[7][2] = DEAD;
        grille[7][3] = DEAD;
        grille[7][4] = DEAD;

        grille[8][0] = DEAD;
        grille[8][1] = DEAD;
        grille[8][2] = DEAD;
        grille[8][3] = DEAD;
        grille[8][4] = DEAD;
    }

    if(lvl == 4)
    {

        grille[0][0] = DEAD;
        grille[0][1] = DEAD;
        grille[0][2] = DEAD;
        grille[0][3] = DEAD;
        grille[0][4] = DEAD;

        grille[1][0] = DEAD;
        grille[1][1] = DEAD;
        grille[1][2] = DEAD;
        grille[1][3] = DEAD;
        grille[1][4] = DEAD;

        grille[2][0] = STANDARD;
        grille[2][1] = DEAD;
        grille[2][2] = GLACE;
        grille[2][3] = DEAD;
        grille[2][4] = DORMEUR;

        grille[3][0] = DEAD;
        grille[3][1] = DEAD;
        grille[3][2] = DEAD;
        grille[3][3] = DEAD;
        grille[3][4] = DEAD;

        grille[4][0] = DEAD;
        grille[4][1] = DEAD;
        grille[4][2] = DEAD;
        grille[4][3] = DEAD;
        grille[4][4] = DEAD;

        grille[5][0] = DEAD;
        grille[5][1] = DEAD;
        grille[5][2] = DEAD;
        grille[5][3] = DEAD;
        grille[5][4] = DEAD;

        grille[6][0] = DEAD;
        grille[6][1] = DEAD;
        grille[6][2] = DEAD;
        grille[6][3] = DEAD;
        grille[6][4] = DEAD;

        grille[7][0] = DEAD;
        grille[7][1] = DEAD;
        grille[7][2] = DEAD;
        grille[7][3] = DEAD;
        grille[7][4] = DEAD;

        grille[8][0] = DEAD;
        grille[8][1] = DEAD;
        grille[8][2] = DEAD;
        grille[8][3] = DEAD;
        grille[8][4] = DEAD;
    }

    if(lvl == 5)
    {
        grille[0][0] = DEAD;
        grille[0][1] = DORMEUR;
        grille[0][2] = DEAD;
        grille[0][3] = DEAD;
        grille[0][4] = FLECHE;

        grille[1][0] = DEAD;
        grille[1][1] = GLACE;
        grille[1][2] = DEAD;
        grille[1][3] = DEAD;
        grille[1][4] = DEAD;

        grille[2][0] = DEAD;
        grille[2][1] = DEAD;
        grille[2][2] = DEAD;
        grille[2][3] = DEAD;
        grille[2][4] = DEAD;

        grille[3][0] = DEAD;
        grille[3][1] = DEAD;
        grille[3][2] = DEAD;
        grille[3][3] = DEAD;
        grille[3][4] = DEAD;

        grille[4][0] = STANDARD;
        grille[4][1] = DEAD;
        grille[4][2] = LIVRE;
        grille[4][3] = DEAD;
        grille[4][4] = DEAD;

        grille[5][0] = DEAD;
        grille[5][1] = DEAD;
        grille[5][2] = DEAD;
        grille[5][3] = LIVRE;
        grille[5][4] = DORMEUR;

        grille[6][0] = DEAD;
        grille[6][1] = DEAD;
        grille[6][2] = LIVRE;
        grille[6][3] = DEAD;
        grille[6][4] = DEAD;

        grille[7][0] = DEAD;
        grille[7][1] = DEAD;
        grille[7][2] = DORMEUR;
        grille[7][3] = DEAD;
        grille[7][4] = DEAD;

        grille[8][0] = DEAD;
        grille[8][1] = DEAD;
        grille[8][2] = DEAD;
        grille[8][3] = DEAD;
        grille[8][4] = GLACE;
    }
}

/****************** Nom de la fonction **********************
* afficherGrille                                            *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 01/12/16                                *
********************* Description ***************************
* Cette fonction affiche les monstres initialisés dans la   *
* grille
*********************** Entrées *****************************
* la grille du niveau, structure Element, SDL_Surface screen
* pour l'affiche
*********************** Sorties *****************************
* Renvoie la grille, e                                      *
************************************************************/
void afficherGrille(TGrille &grille, Element &e, SDL_Surface *screen)
{
    int l,c;
    for (l = 0; l < nbLigne; l++)
    {
        for (c =0; c < nbColonne; c++)
        {
            lectureElement(e, l, c );
            if (grille[l][c]==STANDARD)
            {
                e.lecture_courant = e.lecture_standard;
            }

            if (grille[l][c]==DORMEUR)
            {
                e.lecture_courant = e.lecture_dormeur;
            }

            if (grille[l][c]==LIVRE)
            {
                e.lecture_courant = e.lecture_livre;
            }

            if (grille[l][c]==GLACE)
            {
                e.lecture_courant = e.lecture_glace;
            }

            if (grille[l][c]==DEAD)
            {
                e.lecture_courant = e.lecture_dead;
            }

            if (grille[l][c]==FLECHE)
            {
                e.lecture_courant = e.lecture_fleche;
            }
            applySurface(e.x,e.y,e.element,screen,&e.lecture_courant);

        }
    }
}
/****************** Nom de la fonction **********************
* lvlFini                                                   *
******************** Auteur , Dates *************************
* SATGER / 13/12/16                                         *
********************* Description ***************************
* Cette fonction parcourt la grille et vérifie si elle
* contient des dormeurs
*********************** Entrées *****************************
* TGrille grille                                            *
*********************** Sorties *****************************
* Renvoie vrai si il n'y a pas de dormeur et faux sinon     *
************************************************************/
bool lvlFini(TGrille grille)
{
    int l, c;
    bool present;
    for (l = 0 ; l < nbLigne ;l++)
    {
        for (c = 0 ; c < nbColonne ; c++)
        {
            if ( grille[l][c] == DORMEUR)
            {
                present = true;
            }
        }
    }

    if (!present)
    {
        return true;
    }

    else
    {
        return false;
    }
}


/****************** Nom de la fonction **********************
* decalageDroite                                            *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 12/12/16                                *
********************* Description ***************************
* Affect un déplacement au monstre sélectionné grâce a la
* fonction selecCase
*********************** Entrées *****************************
* grille, l,c : ligne et colonne du monstre sélectionné,    *
* bool droite: permet au monstre de sortir de la grille si
* aucun obstacle n'est sur sa colonne
*********************** Sorties *****************************
* Renvoie la grille, l, c, droite, bas                      *
************************************************************/
void decalageDroite(TGrille & grille, int &l, int &c, bool &droite, bool &bas)
{
    bool dormeur, glace, fleche;
    dormeur = glace = fleche = false;

    while ((grille[l][c+1] == DEAD || grille[l][c+1]==FLECHE) && c < nbColonne-1 && !dormeur && !glace && !fleche)
    {
        if (grille[l][c]!=FLECHE)
        {
            grille[l][c] = DEAD;
        }
        c++;

        if (grille[l][c+1] == DORMEUR)
        {
            droite = false; // bouléen permettant de vérifier si un déplacement a eu lieu
            dormeur = true;
            grille[l][c+1] = STANDARD;
        }
        if (grille[l][c+1] == GLACE)
        {
            droite = false;
            glace = true;
            grille[l][c+1] = DEAD;
        }

        if (grille[l][c] == FLECHE)
        {
            fleche = true;
            droite = false;
            bas = true;
            decalageBas(grille, l, c, bas);
        }
    }

    if (dormeur)
    {
        grille[l][c] = STANDARD;
    }

    else if (glace)
    {
        grille[l][c] = STANDARD;
    }

    else if (fleche)
    {
        grille[l][c] = STANDARD;
    }

    else
    {
        grille[l][c] = STANDARD;
    }
}

/****************** Nom de la fonction **********************
* decalageGauche                                            *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 12/12/16                                *
********************* Description ***************************
* Affect un déplacement au monstre sélectionné grâce a la
* fonction selecCase
*********************** Entrées *****************************
* grille, l,c : ligne et colonne du monstre sélectionné,    *
* bool gauche: permet au monstre de sortir de la grille si
* aucun obstacle n'est sur sa colonne
*********************** Sorties *****************************
* Renvoie la grille, l, c, gauche et bas                    *
************************************************************/
void decalageGauche(TGrille & grille, int &l, int &c, bool &gauche, bool &bas)
{
    bool dormeur, glace, fleche;
    dormeur = glace = fleche = false;

    while ((grille[l][c-1] == DEAD || grille[l][c-1]==FLECHE) && c > 0 && !dormeur && !glace)
    {
        grille[l][c]=DEAD;
        c--;

        if (grille[l][c-1] == DORMEUR)
        {
            dormeur = true;
            gauche = false;
            grille[l][c-1] = STANDARD;
        }

        if (grille[l][c-1] == GLACE)
        {
            glace = true;
            gauche = false;
            grille[l][c-1] = DEAD;
        }

        if (grille[l][c] == FLECHE)
        {
            fleche = true;
            gauche = false;
            bas = true;
            decalageBas(grille, l, c, bas);
        }
    }

    if (dormeur)
    {
        grille[l][c] = STANDARD;
    }

    else if (glace)
    {
        grille[l][c] = STANDARD;
    }

    else if (fleche)
    {
        grille[l][c] = STANDARD;
    }

    else
    {
        grille[l][c] = STANDARD;
    }
}


/****************** Nom de la fonction **********************
* decalageHaut                                              *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 12/12/16                                *
********************* Description ***************************
* Affect un déplacement au monstre sélectionné grâce a la
* fonction selecCase
*********************** Entrées *****************************
* grille, l,c : ligne et colonne du monstre sélectionné,    *
* bool haut: permet au monstre de sortir de la grille si
* aucun obstacle n'est sur sa ligne
*********************** Sorties *****************************
* Renvoie la grille, l, c, haut                             *
************************************************************/
void decalageHaut(TGrille & grille, int &l, int &c, bool &haut)
{
    bool dormeur, glace;
    dormeur = glace = false;

    while (grille[l-1][c] == DEAD && l > 0 && !dormeur && !glace)
    {
        grille[l][c]=DEAD;
        l--;

        if (grille[l-1][c] == DORMEUR)
        {
            dormeur = true;
            haut = false;
            grille[l-1][c] = STANDARD;
        }

        if (grille[l-1][c] == GLACE)
        {
            glace = true;
            haut = false;
            grille[l-1][c] = DEAD;
        }
    }

    if (dormeur)
    {
        grille[l][c] = STANDARD;
    }

    else if (glace)
    {
        grille[l][c] = STANDARD;
    }

    else
    {
        grille[l][c] = STANDARD;
    }
}

/****************** Nom de la fonction **********************
* decalageBas                                               *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 12/12/16                                *
********************* Description ***************************
* Affect un déplacement au monstre sélectionné grâce a la
* fonction selecCase
*********************** Entrées *****************************
* grille, l,c : ligne et colonne du monstre sélectionné,    *
* bool bas: permet au monstre de sortir de la grille si
* aucun obstacle n'est sur sa ligne
*********************** Sorties *****************************
* Renvoie la grille, l, c, bas                              *
************************************************************/
void decalageBas(TGrille & grille, int &l, int &c, bool &bas)
{
    bool dormeur, glace;
    dormeur = glace = false;

    while ((grille[l+1][c] == DEAD || grille[l+1][c] == FLECHE) && l < nbLigne-1 && !dormeur && !glace)
    {
        if (grille[l][c]!=FLECHE)
        {
            grille[l][c] = DEAD;
        }
        l++;

        if (grille[l+1][c] == DORMEUR && l < nbLigne-1)
        {
            dormeur = true;
            bas = false;
            grille[l+1][c] = STANDARD;
        }

        if (grille[l+1][c] == GLACE && l < nbLigne-1)
        {
            bas = false;
            glace = true;
            grille[l+1][c] = DEAD;
        }
    }

    if (dormeur)
    {
        grille[l][c] = STANDARD;
    }

    else if (glace)
    {
        grille[l][c] = STANDARD;
    }

    else
    {
        grille[l][c] = STANDARD;
    }
}

/****************** Nom de la fonction **********************
* selecCase                                                 *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 03/12/16                                *
********************* Description ***************************
* Cette fonction permet d'obtenir les coordonnées d'une case
* en fonction de la position de la souris
*********************** Entrées *****************************
* SDL_Event pour les événements, tmp_l et tmp_c pour stocker
* la ligne et la colonne de la case
*********************** Sorties *****************************
* Renvoie l et c                                            *
************************************************************/
void selecCase( SDL_Event event, int &tmp_l, int &tmp_c)
{
    int sourisY = event.button.y; //coordonées de la souris
    int sourisX = event.button.x;

    while (sourisY > (topBorder + (CASE_HEIGHT  * tmp_l))) //calcul de la ligne et de la colonne correspondant aux coordonnées de la souris
    {
        tmp_l++;
    }

    while (sourisX > (leftBorder + (CASE_WIDTH  * tmp_c)))
    {
        tmp_c++;
    }

    tmp_l=tmp_l-1;
    tmp_c=tmp_c-1;

}


/****************** Nom de la fonction **********************
* compareGrille                                             *
******************** Auteur , Dates *************************
* MESLIN / 03/12/16                                         *
********************* Description ***************************
* Cette fonction permet de tester si l'utilisateur clique
* sur une case contenant un monstre ou pas
*********************** Entrées *****************************
* tmp_l et tmp_c: ligne et colonne de la case, grille       *
*********************** Sorties *****************************
* Renvoie 1 si on clique sur une case et 0 sinon, la grille *
************************************************************/
bool compareGrille(int tmp_l, int tmp_c, TGrille &grille)
{
    if (grille[tmp_l][tmp_c]==STANDARD)
    {
        return true;
    }

    else
    {
        return false;
    }

}

/****************** Nom de la fonction **********************
* gameOver                                                  *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 12/12/16                                *
********************* Description ***************************
* Cette fonction permet de tester si un monstre sort de la
* grille
*********************** Entrées *****************************
* La grille, l et c : ligne et colonne du mosntre,          *
* lvl: niveau, bool bas, droite, haut, gauche: direction du
* monstre
*********************** Sorties *****************************
* Renvoie la grille                                          *
************************************************************/
void gameOver(TGrille &grille, int l, int c, int lvl, bool haut, bool bas, bool droite, bool gauche)
{
    if (haut == true && grille[0][c]==STANDARD)
    {
        initLvl(grille, lvl); // recommence le niveau si le monstre est sorti
    }

    else if (bas == true && grille[nbLigne-1][c]==STANDARD)
    {
        initLvl(grille, lvl);
    }

    else if (gauche == true && grille[l][0]==STANDARD)
    {
        initLvl(grille, lvl);
    }

    else if (droite == true && grille[l][nbColonne-1]==STANDARD)
    {
        initLvl(grille, lvl);
    }
}


/****************** Nom de la fonction **********************
* glisser                                                   *
******************** Auteur , Dates *************************
* SATGER - MESLIN / 14/12/16                                *
********************* Description ***************************
* Cette fonction définie les déplacements en fonction des
* positions stockées de la souris
*********************** Entrées *****************************
* La grille, l et c : ligne et colonne du monstre,          *
* startX, startY: position de début du glissé de la souris
* endX, endY: position de fin du glissé de la souris
*********************** Sorties *****************************
* Renvoie l, c, endX, endY, startX, startY, droite, gauche,
* haut, bas                  *
************************************************************/
void glisser(TGrille &grille, int &l, int &c, int &endX, int &endY, int &startX, int &startY, bool &droite, bool &bas, bool &gauche, bool &haut )
{
    if ((endX-startX) > 0 &&  abs(endX-startX) >  abs(endY-startY))
    {
        droite = true;
        decalageDroite(grille, l, c, droite, bas); // effectue un décalage en fonction de la direction prise par le cliquer-glisser
    }

    else if ((endX-startX) < 0 && abs(endX-startX) > abs(endY-startY))
    {
        gauche = true;
        decalageGauche(grille, l, c, gauche, bas);
    }

    else if ((endY-startY) > 0 && abs(endX-startX) < abs(endY-startY))
    {
        bas = true;
        decalageBas(grille, l, c, bas);
    }

    else if ((endY-startY) < 0 && abs(endX-startX) < abs(endY-startY))
    {
        haut = true;
        decalageHaut(grille, l, c, haut);
    }
}

